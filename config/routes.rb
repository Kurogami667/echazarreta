Rails.application.routes.draw do
  devise_for :users
  get 'dashboard', to: 'dashboard#show'
  root 'home#show'

  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
end
