# == Schema Information
#
# Table name: roles
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  slug       :string
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_roles_on_deleted_at  (deleted_at)
#

class Role < ApplicationRecord
  has_one :user, inverse_of: :role

  validates :name, presence: true, uniqueness: true
end
